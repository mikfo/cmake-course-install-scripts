#!/bin/bash
mkdir -p ~/cmake-course/my-solutions
cd ~/cmake-course
git clone https://gitlab.com/ribomation-courses/build-tools/cmake.git gitlab
mkdir ~/cmake-course/test
cd ~/cmake-course/test
cmake -GNinja ~/cmake-course/gitlab/solutions/ch03-installation
ninja
./hello
