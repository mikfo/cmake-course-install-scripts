#/bin/bash

wget https://github.com/Kitware/CMake/releases/download/v3.16.0-rc3/cmake-3.16.0-rc3.tar.gz
tar xf cmake-3.16.0-rc3.tar.gz
mkdir build-cmake-3.16.0-rc3
cd build-cmake-3.16.0-rc3
../cmake-3.16.0-rc3/bootstrap && make && sudo make install
